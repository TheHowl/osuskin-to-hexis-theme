<!DOCTYPE html>
<html>
<head>
	<title>osu! skin to Hexis theme converter</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
	<?php include "inc/navbar.php"; ?>
	<div class="container">
		<h1 class="page-header">osu!skin to Hexis theme converter</h1>
		<form enctype="multipart/form-data" action="convert.php" method="POST">
			osk or zip of the osu! skin: 
			<input name="uploadfile" id="uploadfile" class="inputbox autowidth" type="file"></input><br />
			Please note: this is an automated service. The sliderball animations won't work, and also the second set of number won't work if it's different from default-[number].png. Also, if the spinner looks strange, blame franceso149, not me.
			<br><button type="submit" class="btn btn-info">Convert!</button>
		</form>
		<h1 class="page-header">How should the zip file be</h1>
		<p>(yes we currently support only zip if you didn't know. And osk, that are still zip. rar will be implemented hopefully soon)</p>
		<p>A lot of times I see people complaining about the fact that they can't convert a skin. Then I get into the directory where all is changed, and I see that they're doing it all wrong. This is how the zip must be structured, otherwise it won't work.</p>
		<pre>filezip (this is the file itself, not a folder)
	filezip/pngfile.png
	filezip/anotherskinpngfile.png

This won't work:
	filezip
	filezip/filezip (the second one is a folder)
	filezip/filezip/content.png

And not even this:
	filezip
	filezip/anyrandomfolderfaggotwithunrelatedcontent
	filezip/content.png
Actually, the one right above will work, but it won't delete the folder itself so please don't.</pre>
		<p>This will hopefully change soon, deleting more correctly directories and supporting zip files that have a folder and then the files. For the moment, just change your zip.</p>
	</div>
	<!-- Bootstrap JS -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>