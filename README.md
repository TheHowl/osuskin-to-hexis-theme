# osu!skin to Hexis theme
### What is this?

This is a converter. It changes the name of the files of a zip folder in order to make them ok for use in Hexis.

### Where can I find it?

[Right here.](http://osu2hexis.thehowl.it)

### How can I contribute?

There are a few development branches that I need to do. It would be really nice if you could help with them!

The actual branches that need to be finished are:

* rar-support
* skintothemexml

You can know what they should do simply by their names. Once you're done, just create a pull request and you're done!

### I can't manage to make it work! :'(

Have a chat with me when I'm in-game, on Hexis (TheHowl, also Howl on IRC) or osu! (Howl). I will try to figure out a solution asap!