<?php
$timeatstart = microtime(true);
// What is this variable for, you may ask? It is to calculate, at the end of the script, the execution time.
// Sets max filesize of 50mb.
ini_set("post_max_size", 52429900);
ini_set("upload_max_size", 52428800);
// function from: http://davidwalsh.name/create-zip-php
function create_zip($files = array(),$destination = '',$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$zip->addFile($file,$file);
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{
		return false;
	}
}
// Generates directory and moves file to dolan/
for($code_length = 8, $newcode = ''; strlen($newcode) < $code_length; $newcode .= chr(!rand(0, 2) ? rand(48, 57) : (!rand(0, 1) ? rand(65, 90) : rand(97, 122))));
// Try code from: http://php.net/manual/en/features.file-upload.php (notes)
mkdir("dolan/" . $newcode . "");
$arrayHexisNames = array(	"healthbar-bg.png",
							"healthbar-bg@2x.png",
							"healthbar-meter.png",
							"healthbar-meter@2x.png",
							"hitlighting.png",
							"hitlighting@2x.png",
							"object-0.png",
							"object-1.png",
							"object-2.png",
							"object-3.png",
							"object-4.png",
							"object-5.png",
							"object-6.png",
							"object-7.png",
							"object-8.png",
							"object-9.png",
							"object-0@2x.png",
							"object-1@2x.png",
							"object-2@2x.png",
							"object-3@2x.png",
							"object-4@2x.png",
							"object-5@2x.png",
							"object-6@2x.png",
							"object-7@2x.png",
							"object-8@2x.png",
							"object-9@2x.png",
							"slider-reversearrow.png",
							"slider-reversearrow@2x.png",
							"slider-ball.png",
							"slider-ball@2x.png",
							"slider-followcircle.png",
							"slider-followcircle@2x.png",
							"slider-tick.png",
							"slider-tick@2x.png",
							"spinner-outline.png",
							"spinner-outline@2x.png",
							"spinner-overlay.png",
							"spinner-overlay@2x.png",
							"spinner-pump.png",
							"spinner-pump@2x.png",
							"spinner.png",
							"spinner@2x.png",
							"warningarrow.png",
							"warningarrow@2x.png");
$arrayosuNames = array(		"scorebar-bg.png",
							"scorebar-bg@2x.png",
							"scorebar-colour.png",
							"scorebar-colour@2x.png",
							"lighting.png",
							"lighting@2x.png",
							"default-0.png",
							"default-1.png",
							"default-2.png",
							"default-3.png",
							"default-4.png",
							"default-5.png",
							"default-6.png",
							"default-7.png",
							"default-8.png",
							"default-9.png",
							"default-0@2x.png",
							"default-1@2x.png",
							"default-2@2x.png",
							"default-3@2x.png",
							"default-4@2x.png",
							"default-5@2x.png",
							"default-6@2x.png",
							"default-7@2x.png",
							"default-8@2x.png",
							"default-9@2x.png",
							"reversearrow.png",
							"reversearrow@2x.png",
							"sliderb0.png",
							"sliderb0@2x.png",
							"sliderfollowcircle.png",
							"sliderfollowcircle@2x.png",
							"sliderscorepoint.png",
							"sliderscorepoint@2x.png",
							"spinner-approachcircle.png",
							"spinner-approachcircle@2x.png",
							"spinner-metre.png",
							"spinner-metre@2x.png",
							"spinner-background.png",
							"spinner-background@2x.png",
							"spinner-circle.png",
							"spinner-circle@2x.png",
							"play-warningarrow.png",
							"play-warningarrow@2x.png");
$zip = new ZipArchive;
$extractpath = 'dolan/' . $newcode . '/';
if ($zip->open($_FILES['uploadfile']['tmp_name']) === true) {
	$zip->extractTo($extractpath);
    $zip->close();
}
for ($inde = 0; $inde < count($arrayHexisNames); $inde++) {
	if (file_exists('dolan/' . $newcode . '/' . $arrayosuNames[$inde])) {
		rename('dolan/' . $newcode . '/' . $arrayosuNames[$inde], 'dolan/' . $newcode . '/' . $arrayHexisNames[$inde]);
	}
}
// finds all the filenames of the directory
$dirfiles = glob("dolan/". $newcode . "/*.*");
create_zip($dirfiles, 'finished/' . $newcode . '.zip', true);
foreach ($dirfiles as $tiziocaio82) {
	unlink($tiziocaio82);
}
unset($tiziocaio82);
rmdir('dolan/' . $newcode);
$timefinish = microtime(true) - $timeatstart;
?>
<!DOCTYPE html>
<html>
<head>
	<title>Finished (probably)</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>
	<?php include "inc/navbar.php"; ?>
	<div class="container">
		Seems to be finished? Here you are: <a href="finished/<?php echo $newcode; ?>.zip" target="_blank">click here</a><br>
		If php says some errors, then probably it isn't ready. Send an MP to Howl/TheHowl/DahHowl somewhere, and tell him about the issue.<br>
		I HATE OVH SO MUCH<br>
		Page execution time (milliseconds): <?php echo $timefinish; ?><br>
	</div>
	<!-- Bootstrap JS -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>